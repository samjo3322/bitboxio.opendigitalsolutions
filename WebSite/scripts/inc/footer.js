/*This script assumes browser.js is loaded in the page.*/

var footer = '';

if(!TVC.isIE()){
	
	footer += '<!-- BEGIN FOOTER -->';
	footer += '<div id="footer">';
	footer += 	'<div id="copyright">';
	footer += 		'&copy; 2013 Sam Crouch. All rights reservered.';
	footer += 	'</div>';
	footer += '</div>';
	footer += '<!-- END FOOTER -->';

}else{
	
	footer += '<!-- BEGIN FOOTER -->';
	footer += '<div id="footer-cap"></div>';
	footer += '<div id="footer">';
	footer += 	'<div id="copyright">';
	footer += 		'&copy; 2013 Sam Crouch. All rights reservered.';
	footer += 	'</div>';
	footer += '</div>';
	footer += '<!-- END FOOTER -->';

}

document.write(footer);




/*This script assumes browser.js is loaded in the page.*/

var nav = '';

if(!TVC.isIE()){
	nav += '<!-- BEGIN NAV -->';
	nav += '<div id="nav">';
	nav += 	'<div id="nav-area">';
	nav += 	'<img src="./assets/images/logo.png" align="left">';
	nav += '<div id="nav-tagline">Open Digital Solutions</div>';
	nav += 		'<div class="tab" onclick="window.location=\'contact.html\'">';
	nav += 			'<div class="tablabel" for="tab-4">Contact</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'bio.html\'">';
	nav += 			'<div class="tablabel" for="tab-3">About</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'planners.html\'">';
	nav += 			'<div class="tablabel" for="tab-2">Meeting Planners</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'media.html\'">';
	nav += 			'<div class="tablabel" for="subtab-1">Press</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'index.html\'">';
	nav += 			'<div class="tablabel" for="subtab-0">Home</div>';
	nav += 		'</div>';
	nav += 	'</div>';
	nav += '</div>';
	nav += '<!-- END NAV -->';

	document.write(nav);
}else{
	nav += '<!-- BEGIN NAV -->';
	nav += '<div id="nav">';
	
	nav += 	'<div id="nav-area">';
	nav += 	'<img src="./assets/images/logo.png" align="left">';
	nav += '<div id="nav-tagline">Open Digital Solutions</div>';
	nav += 		'<div class="tab" onclick="window.location=\'contact.html\'">';
	nav += 			'<input type="radio" id="tab-4" name="tab-group-1">';
	nav += 			'<div class="tablabel" for="tab-4">Contact</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'bio.html\'">';
	nav += 			'<input type="radio" id="tab-3" name="tab-group-1">';
	nav += 			'<div class="tablabel" for="tab-3">About</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'planners.html\'">';
	nav += 			'<input type="radio" id="tab-2" name="tab-group-1">';
	nav += 			'<div class="tablabel" for="tab-2">Meeting Planners</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'media.html\'">';
	nav += 			'<input type="radio" id="tab-1" name="subtab-group-1">';
	nav += 			'<div class="tablabel" for="subtab-1">Press</div>';
	nav += 		'</div>';
	nav += 		'<div class="tab" onclick="window.location=\'index.html\'">';
	nav += 			'<input type="radio" id="tab-0" name="subtab-group-1">';
	nav += 			'<div class="tablabel" for="subtab-0">Home</div>';
	nav += 		'</div>';
	nav += 	'</div>';
	nav += '</div>';
	nav += '<!-- END NAV -->';

	document.write(nav);
	
	
	switch(siteloc){
		case 'home':
			document.getElementById('tab-0').checked = true;
			break;
		case 'media':
			document.getElementById('tab-1').checked = true;
			break;
		case 'planners':
			document.getElementById('tab-2').checked = true;
			break;
		case 'bio':
			document.getElementById('tab-3').checked = true;
			break;
		case 'contact':
			document.getElementById('tab-4').checked = true;
			break;
	}
	
}



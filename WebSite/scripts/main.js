var TVC = {};

TVC = {
	
	isIE : function(){
		var result = navigator.appVersion.indexOf("MSIE 7.");
		
		if(result >= 1){
			return false;
		}else{
			return true;
		}
	},
	
	togglePopDown : function(){
		var bookarea = document.getElementById('book-area');
		var bookhandle = document.getElementById('book-handle');
		
		if(bookarea.style.top == "0px"){
			bookarea.style.top = "-65px";
			bookhandle.style.top = "-65px";
		}else{
			bookarea.style.top = "0px";
			bookhandle.style.top = "0px";
		}
		
	},
	
	resetInfotabs : function(){
		document.getElementById('infotab1').style.top = "-435px";
		document.getElementById('infotab2').style.top = "-489px";
		document.getElementById('infotab3').style.top = "-543px";
	},
	
	fade : function(eid, TimeToFade){
		var element = document.getElementById(eid);
		
		if(element == null)
			return;
		
		if(element.FadeState == null){
			if(element.style.opacity == null || element.style.opacity == '' || element.style.opacity == '1'){
			  element.FadeState = 2;
			}else{
			  element.FadeState = -2;
			}
		}
		
		if(element.FadeState == 1 || element.FadeState == -1){
			element.FadeState = element.FadeState == 1 ? -1 : 1;
			element.FadeTimeLeft = TimeToFade - element.FadeTimeLeft;
		}else{
			
			element.FadeState = element.FadeState == 2 ? -1 : 1;
			element.FadeTimeLeft = TimeToFade;
			setTimeout("TVC.animateFade(" + new Date().getTime() + ",'" + eid + "')", 33);
		} 
	},
	
	animateFade : function(lastTick, eid){
		var TimeToFade = 1000;
		var curTick = new Date().getTime();
		var elapsedTicks = curTick - lastTick;

		var element = document.getElementById(eid);

		if(element.FadeTimeLeft <= elapsedTicks)
		{
			element.style.opacity = element.FadeState == 1 ? '1' : '0';
			element.style.filter = 'alpha(opacity = '
			+ (element.FadeState == 1 ? '100' : '0') + ')';
		element.FadeState = element.FadeState == 1 ? 2 : -2;
		return;
		}

		element.FadeTimeLeft -= elapsedTicks;
		var newOpVal = element.FadeTimeLeft/TimeToFade;
		if(element.FadeState == 1)
		newOpVal = 1 - newOpVal;

		element.style.opacity = newOpVal;
		element.style.filter = 'alpha(opacity = ' + (newOpVal*100) + ')';

		setTimeout("TVC.animateFade(" + curTick + ",'" + eid + "')", 33);
	},
	
	searchKeyPress : function(e)
    {
        // look for window.event in case event isn't passed in
        if (typeof e == 'undefined' && window.event) { e = window.event; }
        if (e.keyCode == 13)
        {
            document.getElementById('btnSubmit').click();
        }
    },
	
	resetContactForm : function(){
		document.getElementById('contact-name').value = 'Name';
		document.getElementById('contact-email').value = 'Email Address';
		document.getElementById('contact-phone').value = 'Phone Number';
		document.getElementById('contact-message').value = 'Questions or Comments';
	},
	
	handleContactForm : function(ref){
		var name = document.getElementById('contact-name').value;
		var email = document.getElementById('contact-email').value;
		var phone = document.getElementById('contact-phone').value;
		var msg = document.getElementById('contact-message').value;

		if(name == '' || email == '' || phone == '' || msg == ''){  
			alert("please fill out all fields.");  
		}else{  
			var sendto = '';
			var subject = '';
			var body = '';
			body += 'Name: ' + name + escape('\n');
			body += 'Email Address: ' + email + escape('\n');
			body += 'Phone Number: ' + phone + escape('\n');
			body += escape('\n');
			body += msg;
			body = body.replace(' ', '%20'); 
			
			switch(ref){
				default:
					sendto = 'ebunten';
					subject = 'CONTACT';
					break;
			};

			window.location.href = "ma"+"ilto:"+sendto+"@"+"thevisibilitycompany.com"+"?subject="+subject+"&body="+body; 
		}
	}
};

